const express = require('express');
const router = express.Router();
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const setting=require("../validation/settings");
const Page_cont=require("./return_msg/setting");
//const passport = require('passport-local');
var nodemailer = require('nodemailer');
var upload=require('express-fileupload')
const nodeMailer = require('nodemailer');
const config = require('../config/keys.js');

//Load Input Validation
const validationEnroll = require('../validation/enrollValidation');
const validationStudentUpdate = require('../validation/updateStudentValidation');
let middleware = require('../validation/middleware');
const Role_authority = require('../models/role_authority');
var generator = require('generate-password');


const Student = require('../models/student_enrollment');
    var mobileToSave;

// const loginStatus = require('../models/login_status');



// *** POST *** /api/users/register *** Create new student enrollment ***
router.post("/enroll/new", (req, res) => {
    console.log(req.body);
    console.log("------------");

    var validationResult = validationEnroll.checkEnrollValidation(req.body);

    if (!validationResult.status)
    {
        return res.send(validationResult);
    }
    console.log("1");

    var enteredMobile = req.body.contact_no;

    mobileToSave = '91'+ enteredMobile;

    Student.findOne({email: req.body.email})
    .then(student =>
        {
          console.log("2");

            if (student)
            {
                res.status(200).send(setting.status("Email Already exits", false, "Email unique", null))
            }
            else
            {
              console.log("3");

              Student.findOne({contact_no: req.body.contact_no})
              .then(student =>
                {
                  if (student)
                  {
                    res.status(200).send(setting.status("Contact Already exits", false, "Contact unique", null))
                  }
                  else
                  {
                    const newStudent = new Student({
                      fname: req.body.fname,
                      lname: req.body.lname,
                      contact_no: mobileToSave,
                      college_id: req.body.college_id,
                      course_id: req.body.course_id,
                      email: req.body.email,
                      payment_status:"not_paid",
                      password:mobileToSave,
                      role:"5c2f0e9243abb4222c581d42",
                    });
                            
                    console.log("4");

                    bcrypt.genSalt(10, (err, salt) =>
                    {
                        bcrypt.hash(newStudent.password, salt, (err, hash) =>
                        {
                            if (err)
                                throw err;
                                newStudent.password = hash;

                                newStudent.save()
                    // .then(student => res.status(200).send(setting.status("Student Enrollment Successfully", true, "Student Enrolled", student._id)))
                    .then(student => 
                      {
                        console.log("Student data saved goto send mail");
						
						if(student)
						{
							var transporter = nodemailer.createTransport({
							  service: 'gmail',
							  auth: {
								user: 'samplejobportal@gmail.com',
								pass: 'Job@1234'
							  }
							});

							var mailOptions = {
							  from: 'samplejobportal@gmail.com',
							  to: req.body.email,
							  subject: 'Successfully Enrolled',
							  html: `<!DOCTYPE html><html><head> <meta charset="utf-8"> <meta http-equiv="x-ua-compatible" content="ie=edge"> <title>Password Reset</title> <meta name="viewport" content="width=device-width, initial-scale=1"> <style type="text/css"> /** * Google webfonts. Recommended to include the .woff version for cross-client compatibility. */ @media screen{@font-face{font-family: 'Source Sans Pro'; font-style: normal; font-weight: 400; src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format('woff');}@font-face{font-family: 'Source Sans Pro'; font-style: normal; font-weight: 700; src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format('woff');}}/** * Avoid browser level font resizing. * 1. Windows Mobile * 2. iOS / OSX */ body, table, td, a{-ms-text-size-adjust: 100%; /* 1 */ -webkit-text-size-adjust: 100%; /* 2 */}/** * Remove extra space added to tables and cells in Outlook. */ table, td{mso-table-rspace: 0pt; mso-table-lspace: 0pt;}/** * Better fluid images in Internet Explorer. */ img{-ms-interpolation-mode: bicubic;}/** * Remove blue links for iOS devices. */ a[x-apple-data-detectors]{font-family: inherit !important; font-size: inherit !important; font-weight: inherit !important; line-height: inherit !important; color: inherit !important; text-decoration: none !important;}/** * Fix centering issues in Android 4.4. */ div[style*="margin: 16px 0;"]{margin: 0 !important;}body{width: 100% !important; height: 100% !important; padding: 0 !important; margin: 0 !important;}/** * Collapse table borders to avoid space between cells. */ table{border-collapse: collapse !important;}a{color: #1a82e2;}img{height: auto; line-height: 100%; text-decoration: none; border: 0; outline: none;}</style></head><body style="background-color: #e9ecef;"> <div class="preheader" style="display: none; max-width: 0; max-height: 0; overflow: hidden; font-size: 1px; line-height: 1px; color: #fff; opacity: 0;"> Enrollment successfully. </div><table border="0" cellpadding="0" cellspacing="0" width="100%"> <tr> <td align="center" bgcolor="#e9ecef"><!--[if (gte mso 9)|(IE)]> <table align="center" border="0" cellpadding="0" cellspacing="0" width="600"> <tr> <td align="center" valign="top" width="600"><![endif]--> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;"> <tr> <td align="center" valign="top" style="padding: 36px 24px;"> <a href="https://google.com" target="_blank" style="display: inline-block;"> <img src="http://172.104.40.142:3000/img/brand/logo-white.png" alt="Logo" border="0" width="200" style="display: block; width: 200px; max-width: 200px; min-width: px;"> </a> </td></tr></table><!--[if (gte mso 9)|(IE)]> </td></tr></table><![endif]--> </td></tr><tr> <td align="center" bgcolor="#e9ecef"><!--[if (gte mso 9)|(IE)]> <table align="center" border="0" cellpadding="0" cellspacing="0" width="600"> <tr> <td align="center" valign="top" width="600"><![endif]--> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;"> <tr> <td align="left" bgcolor="#ffffff" style="padding: 36px 24px 0; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; border-top: 3px solid #d4dadf;"> <h1 style="margin: 0; font-size: 32px; font-weight: 700; letter-spacing: -1px; line-height: 48px;">Successfully Enrolled</h1> </td></tr></table><!--[if (gte mso 9)|(IE)]> </td></tr></table><![endif]--> </td></tr><tr> <td align="center" bgcolor="#e9ecef"><!--[if (gte mso 9)|(IE)]> <table align="center" border="0" cellpadding="0" cellspacing="0" width="600"> <tr> <td align="center" valign="top" width="600"><![endif]--> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;"> <tr> <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;"> <p style="margin: 0;">Hi ` + req.body.email + `, You have successfully enrolled to the CAS Job Placements. To continue to the next procedure please pay ₹500 by clicking this link</p></td></tr><tr> <td align="left" bgcolor="#ffffff"> <table border="0" cellpadding="0" cellspacing="0" width="100%"> <tr> <td align="center" bgcolor="#ffffff" style="padding: 12px;"> <table border="0" cellpadding="0" cellspacing="0"> <tr> <td align="center" bgcolor="#1a82e2" style="border-radius: 6px;"> <a href="https://google.com" target="_blank" style="display: inline-block; padding: 16px 36px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; color: #ffffff; text-decoration: none; border-radius: 6px;">Make Payments</a> </td></tr></table> </td></tr></table> </td></tr></table><!--[if (gte mso 9)|(IE)]> </td></tr></table><![endif]--> </td></tr><tr> <td align="center" bgcolor="#e9ecef" style="padding: 24px;"><!--[if (gte mso 9)|(IE)]> <table align="center" border="0" cellpadding="0" cellspacing="0" width="600"> <tr> <td align="center" valign="top" width="600"><![endif]--> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;"> <tr> <td align="center" bgcolor="#e9ecef" style="padding: 12px 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #666;"> <p style="margin: 0;">CAS SHRC | Bridging Talents and Opportunities</p></td></tr></table><!--[if (gte mso 9)|(IE)]> </td></tr></table><![endif]--> </td></tr></table> </body></html>`
							};

							transporter.sendMail(mailOptions, function(error, info){
							  if (error) {
								console.log(error);
							  } else {
                                res.status(200).send(setting.status("Student Enrollment Successfully, Please Check your Mail", true, "Student Enrolled", student._id))
								console.log('Email sent: ' + info.response);
							  }
							});
						
						
						
						}
					
					
                        // if(student)
                          // {
                            // var transporter = nodemailer.createTransport({
                              // host: 'smtp.zoho.com',
                              // port: 465,
                              // secure: true,
                              // auth: {
                              // service: 'gmail',
                              // auth: {
                                // user: 'samplejobportal@gmail.com',
                                // pass: 'Job@1234'
                                // user: 'placements@casshrc.com',
                                // pass: 'QYFZcrqSG0YE'
                              // }
                            // });

                            // var mailOptions = {
                              // from: 'placements@casshrc.com',
                              // to: req.body.email,
                              // subject: 'Successfully Enrolled',
                              // html: `<body class="" style="background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"> <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;"> <tr> <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td><td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; max-width: 580px; padding: 10px; width: 580px;"> <div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;"> <span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">This is preheader text. Some clients will show this text as a preview.</span> <table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;"> <tr> <td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;"> <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;"> <tr> <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"> <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Hi ` + req.body.fname + `,</p><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">You have successfully enrolled to the CAS Job Placements. To continue to the next procedure please pay ₹500 by clicking this lik</p><table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; box-sizing: border-box;"> <tbody> <tr> <td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 15px;"> <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto;"> <tbody> <tr> <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; background-color: #3498db; border-radius: 5px; text-align: center;"> <a href="http://google.lk" target="_blank" style="display: inline-block; color: #ffffff; background-color: #3498db; border: solid 1px #3498db; border-radius: 5px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-transform: capitalize; border-color: #3498db;">Make Payments</a> </td></tr></tbody> </table> </td></tr></tbody> </table> 
							              // </td></tr></table> </td></tr></table> <div class="footer" style="clear: both; Margin-top: 10px; text-align: center; width: 100%;"> <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;"> <tr> <td class="content-block" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #999999; text-align: center;"> <span class="apple-link" style="color: #999999; font-size: 12px; text-align: center;">CAS SHRC | Bridging Talents and Opportunities</span> <br>Don't like these emails? <a href="http://i.imgur.com/CScmqnj.gif" style="text-decoration: underline; color: #999999; font-size: 12px; text-align: center;">Unsubscribe</a>. </td></tr><tr> <td class="content-block powered-by" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #999999; text-align: center;"> </td></tr></table> </div></div></td><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td></tr></table> </body>`
                            // };

                            // transporter.sendMail(mailOptions, function(error, info)
                            // {
                              // if (error)
                              // {
                                // console.log(error);
                              // } 
                              // else
                              // {
                                // res.status(200).send(setting.status("Student Enrollment Successfully, Please Check your Mail", true, "Student Enrolled", student._id))
                              // }
                            // });
                          // }
                      // })
					  // .catch(err => res.status(200).send(setting.status("Sorry! Try again", false, "Unable to create Enrollment", err)))


                        // })
                     });

                   })
				  
                })
            }
        })
	}
})
})
 

 
// *** POST *** /api/users/userId *** Update student details ***
router.post("/:id", (req, res) => {

  const id = req.params.id;
  if(!id)
  {
    return res.status(200).send(setting.status("Studnet Id not found",false,"invalid id",null));
  }

  console.log(req.body);
  console.log("------------");

  var validationResult = validationStudentUpdate.checkStudentUpdateValidation(req.body);

  if (!validationResult.status)
  {
      return res.send(validationResult);
  }
      var enteredMobile = req.body.contact_no;

      //Mobile length validation
    var mobileLength = Object.keys(req.body.contact_no).length; // Taking length

    if(mobileLength == 10 && (enteredMobile.charAt(0)==0))
    {
        splitNum = enteredMobile.substring(1);
        mobileToSave = '91'+ splitNum;
        console.log("Mobile starts with 0 : " + mobileToSave);
    }

    if(mobileLength == 9 )
    {
        splitNum = enteredMobile;
        mobileToSave = '91'+ enteredMobile;
        console.log("Mobile starts without 0 : " + mobileToSave);
    }

    if(mobileLength == 11 && (enteredMobile.substring(0,2)==91))
    {
        mobileToSave = enteredMobile;
        console.log("Mobile starts with 91 : " + mobileToSave);
    }

    console.log(req.files)


  Student.findOne({_id: id})
  .then(student => {
      if(!student)
      {
        setting.status("Student not found",false,"student not found", null)
      }
      else if (student)
      {
        
          Student.findByIdAndUpdate(id, {
                fname: req.body.fname,
                lname: req.body.lname,
                contact_no: mobileToSave,
                college_id: req.body.college_id,
                course_id: req.body.course_id,
                email: req.body.email,
                age : req.body.age,
                gender :req.body.gender,
                current_address : req.body.current_address,
                permanant_address : req.body.permanant_address,
                project : req.body.project,
                specific_academic_achivement : req.body.specific_academic_achivement,
                academic_from : req.body.academic_from,
                academic_to : req.body.academic_to,
                writen_introduction_question : req.body.writen_introduction_question,
                writen_introduction_answer : req.body.writen_introduction_answer,
                video : req.body.video,
                written_test_result : req.body.written_test_result,
                
        }, {new: true})
              .then(student => {
                  res.json(setting.status("Student Updated", true, "updated", student));
                })
                .catch(err => {
                    res.json(setting.status("Student Not Found", false, "error", err));
                });
            }
            else
            {
                res.json(setting.status("Student Not Found", false, "error", err));
            }
        })
})

// *** POST *** /api/users/userId *** Update student details ***
router.post("/payment_status/:id", (req, res) => {

  const id = req.params.id;
  
  
  //Find student email
  Student.findById(id, 'fname email', function (err, getMailById)
  {
	  
	  var randPassword = generator.generate({
			length: 8,
			numbers: true
		  });
		  
		  console.log(randPassword);
		  
		  //Hashing password 
		  var hashedPassword = bcrypt.hashSync(randPassword, 10); //Hashing password to unreadable
		  
		  

  Student.findOne({_id: id})
  .then(student => {
      if(!student)
      {
        setting.status("Student not found", false, "student not found", null)
      }
      else if (student)
      {
          Student.findByIdAndUpdate(id, {payment_status: req.body.status, password: hashedPassword}, {new: true})
		  .then(student => {
			  // Not send this instead of send mail
              // res.json(setting.status("Student Payment Sucessfully", true, "updated", null));
			  
			 	  
			  
			  if(student)
			{
				var transporter = nodemailer.createTransport({
				  service: 'gmail',
				  auth: {
					user: 'samplejobportal@gmail.com',
					pass: 'Job@1234'
				  }
				});

				var mailOptions = {
				  from: 'samplejobportal@gmail.com',
				  to: getMailById.email,
				  subject: 'Successfully Paid',
				  html: `<!DOCTYPE html><html><head> <meta charset="utf-8"> <meta http-equiv="x-ua-compatible" content="ie=edge"> <title>Paid</title> <meta name="viewport" content="width=device-width, initial-scale=1"> <style type="text/css"> /** * Google webfonts. Recommended to include the .woff version for cross-client compatibility. */ @media screen{@font-face{font-family: 'Source Sans Pro'; font-style: normal; font-weight: 400; src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format('woff');}@font-face{font-family: 'Source Sans Pro'; font-style: normal; font-weight: 700; src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format('woff');}}/** * Avoid browser level font resizing. * 1. Windows Mobile * 2. iOS / OSX */ body, table, td, a{-ms-text-size-adjust: 100%; /* 1 */ -webkit-text-size-adjust: 100%; /* 2 */}/** * Remove extra space added to tables and cells in Outlook. */ table, td{mso-table-rspace: 0pt; mso-table-lspace: 0pt;}/** * Better fluid images in Internet Explorer. */ img{-ms-interpolation-mode: bicubic;}/** * Remove blue links for iOS devices. */ a[x-apple-data-detectors]{font-family: inherit !important; font-size: inherit !important; font-weight: inherit !important; line-height: inherit !important; color: inherit !important; text-decoration: none !important;}/** * Fix centering issues in Android 4.4. */ div[style*="margin: 16px 0;"]{margin: 0 !important;}body{width: 100% !important; height: 100% !important; padding: 0 !important; margin: 0 !important;}/** * Collapse table borders to avoid space between cells. */ table{border-collapse: collapse !important;}a{color: #1a82e2;}img{height: auto; line-height: 100%; text-decoration: none; border: 0; outline: none;}</style></head><body style="background-color: #e9ecef;"> <div class="preheader" style="display: none; max-width: 0; max-height: 0; overflow: hidden; font-size: 1px; line-height: 1px; color: #fff; opacity: 0;"> Successfully paid and login ... </div><table border="0" cellpadding="0" cellspacing="0" width="100%"> <tr> <td align="center" bgcolor="#e9ecef"><!--[if (gte mso 9)|(IE)]> <table align="center" border="0" cellpadding="0" cellspacing="0" width="600"> <tr> <td align="center" valign="top" width="600"><![endif]--> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;"> <tr> <td align="center" valign="top" style="padding: 36px 24px;"> <a href="https://google.com" target="_blank" style="display: inline-block;"> <img src="http://172.104.40.142:3000/img/brand/logo-white.png" alt="Logo" border="0" width="200" style="display: block; width: 200px; max-width: 200px; min-width: px;"> </a> </td></tr></table><!--[if (gte mso 9)|(IE)]> </td></tr></table><![endif]--> </td></tr><tr> <td align="center" bgcolor="#e9ecef"><!--[if (gte mso 9)|(IE)]> <table align="center" border="0" cellpadding="0" cellspacing="0" width="600"> <tr> <td align="center" valign="top" width="600"><![endif]--> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;"> <tr> <td align="left" bgcolor="#ffffff" style="padding: 36px 24px 0; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; border-top: 3px solid #d4dadf;"> <h1 style="margin: 0; font-size: 32px; font-weight: 700; letter-spacing: -1px; line-height: 48px;">Successfully Paid</h1> </td></tr></table><!--[if (gte mso 9)|(IE)]> </td></tr></table><![endif]--> </td></tr><tr> <td align="center" bgcolor="#e9ecef"><!--[if (gte mso 9)|(IE)]> <table align="center" border="0" cellpadding="0" cellspacing="0" width="600"> <tr> <td align="center" valign="top" width="600"><![endif]--> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;"> <tr> <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;"> <p style="margin: 0;">Hi ` + getMailById.fname + `, You have successfully paid for your placements. The following is your login credentials. Change your password after you login.</p><p> <code> email : ` + getMailById.email + `</code> <br/><code> password : ` + randPassword + ` </code> </p></td></tr><tr> <td align="left" bgcolor="#ffffff"> <table border="0" cellpadding="0" cellspacing="0" width="100%"> <tr> <td align="center" bgcolor="#ffffff" style="padding: 12px;"> <table border="0" cellpadding="0" cellspacing="0"> <tr> <td align="center" bgcolor="#1a82e2" style="border-radius: 6px;"> <a href="https://google.com" target="_blank" style="display: inline-block; padding: 16px 36px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; color: #ffffff; text-decoration: none; border-radius: 6px;">Login</a> </td></tr></table> </td></tr></table> </td></tr></table><!--[if (gte mso 9)|(IE)]> </td></tr></table><![endif]--> </td></tr><tr> <td align="center" bgcolor="#e9ecef" style="padding: 24px;"><!--[if (gte mso 9)|(IE)]> <table align="center" border="0" cellpadding="0" cellspacing="0" width="600"> <tr> <td align="center" valign="top" width="600"><![endif]--> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;"> <tr> <td align="center" bgcolor="#e9ecef" style="padding: 12px 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #666;"> <p style="margin: 0;">CAS SHRC | Bridging Talents and Opportunities</p></td></tr></table><!--[if (gte mso 9)|(IE)]> </td></tr></table><![endif]--> </td></tr></table> </body></html>`
				};

				transporter.sendMail(mailOptions, function(error, info){
				  if (error) {
					console.log(error);
				  } else {
					return res.status(200).send(setting.status("Your payment successful, Please Check your Mail", true, "Student Paid", student._id))
					console.log('Email sent: ' + info.response);
				  }
				});
			}
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  // if(student)
			  // {
				// var transporter = nodemailer.createTransport({
				  // for zoho
				 
				  // host: 'smtp.zoho.com',
				  // port: 465,
				  // secure: true,
				  // auth: {				  
					// user: 'placements@casshrc.com',
					// pass: 'QYFZcrqSG0YE'
				  // }
				  
				  
				  // for Gmail
				  // service: "Gmail",
					// auth: {
						// user: "samplejobportal@gmail.com",
						// pass: "Job@1234"
					// }
				// });

				// var mailOptions = {
				  // from: 'placements@casshrc.com',
				  // to: getMailById.email,
				  // subject: 'Successfully Paid',
				  // html: `<!DOCTYPE html><html><head> <meta charset="utf-8"> <meta http-equiv="x-ua-compatible" content="ie=edge"> <title>Paid</title> <meta name="viewport" content="width=device-width, initial-scale=1"> <style type="text/css"> /** * Google webfonts. Recommended to include the .woff version for cross-client compatibility. */ @media screen{@font-face{font-family: 'Source Sans Pro'; font-style: normal; font-weight: 400; src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format('woff');}@font-face{font-family: 'Source Sans Pro'; font-style: normal; font-weight: 700; src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format('woff');}}/** * Avoid browser level font resizing. * 1. Windows Mobile * 2. iOS / OSX */ body, table, td, a{-ms-text-size-adjust: 100%; /* 1 */ -webkit-text-size-adjust: 100%; /* 2 */}/** * Remove extra space added to tables and cells in Outlook. */ table, td{mso-table-rspace: 0pt; mso-table-lspace: 0pt;}/** * Better fluid images in Internet Explorer. */ img{-ms-interpolation-mode: bicubic;}/** * Remove blue links for iOS devices. */ a[x-apple-data-detectors]{font-family: inherit !important; font-size: inherit !important; font-weight: inherit !important; line-height: inherit !important; color: inherit !important; text-decoration: none !important;}/** * Fix centering issues in Android 4.4. */ div[style*="margin: 16px 0;"]{margin: 0 !important;}body{width: 100% !important; height: 100% !important; padding: 0 !important; margin: 0 !important;}/** * Collapse table borders to avoid space between cells. */ table{border-collapse: collapse !important;}a{color: #1a82e2;}img{height: auto; line-height: 100%; text-decoration: none; border: 0; outline: none;}</style></head><body style="background-color: #e9ecef;"> <div class="preheader" style="display: none; max-width: 0; max-height: 0; overflow: hidden; font-size: 1px; line-height: 1px; color: #fff; opacity: 0;"> A preheader is the short summary text that follows the subject line when an email is viewed in the inbox. </div><table border="0" cellpadding="0" cellspacing="0" width="100%"> <tr> <td align="center" bgcolor="#e9ecef"><!--[if (gte mso 9)|(IE)]> <table align="center" border="0" cellpadding="0" cellspacing="0" width="600"> <tr> <td align="center" valign="top" width="600"><![endif]--> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;"> <tr> <td align="center" valign="top" style="padding: 36px 24px;"> <a href="https://google.com" target="_blank" style="display: inline-block;"> <img src="http://172.104.40.142:3000/img/brand/logo-white.png" alt="Logo" border="0" width="200" style="display: block; width: 200px; max-width: 200px; min-width: px;"> </a> </td></tr></table><!--[if (gte mso 9)|(IE)]> </td></tr></table><![endif]--> </td></tr><tr> <td align="center" bgcolor="#e9ecef"><!--[if (gte mso 9)|(IE)]> <table align="center" border="0" cellpadding="0" cellspacing="0" width="600"> <tr> <td align="center" valign="top" width="600"><![endif]--> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;"> <tr> <td align="left" bgcolor="#ffffff" style="padding: 36px 24px 0; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; border-top: 3px solid #d4dadf;"> <h1 style="margin: 0; font-size: 32px; font-weight: 700; letter-spacing: -1px; line-height: 48px;">Successfully Paid</h1> </td></tr></table><!--[if (gte mso 9)|(IE)]> </td></tr></table><![endif]--> </td></tr><tr> <td align="center" bgcolor="#e9ecef"><!--[if (gte mso 9)|(IE)]> <table align="center" border="0" cellpadding="0" cellspacing="0" width="600"> <tr> <td align="center" valign="top" width="600"><![endif]--> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;"> <tr> <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;"> <p style="margin: 0;">Hi, You have successfully paid for your placements. The following is your login credentials. Change your password after you login.</p><p> <code> email : ` + getMailById.email + `</code> <code> password : ` + randPassword + ` </code> </p></td></tr><tr> <td align="left" bgcolor="#ffffff"> <table border="0" cellpadding="0" cellspacing="0" width="100%"> <tr> <td align="center" bgcolor="#ffffff" style="padding: 12px;"> <table border="0" cellpadding="0" cellspacing="0"> <tr> <td align="center" bgcolor="#1a82e2" style="border-radius: 6px;"> <a href="https://google.com" target="_blank" style="display: inline-block; padding: 16px 36px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; color: #ffffff; text-decoration: none; border-radius: 6px;">Make Payments</a> </td></tr></table> </td></tr></table> </td></tr></table><!--[if (gte mso 9)|(IE)]> </td></tr></table><![endif]--> </td></tr><tr> <td align="center" bgcolor="#e9ecef" style="padding: 24px;"><!--[if (gte mso 9)|(IE)]> <table align="center" border="0" cellpadding="0" cellspacing="0" width="600"> <tr> <td align="center" valign="top" width="600"><![endif]--> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;"> <tr> <td align="center" bgcolor="#e9ecef" style="padding: 12px 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #666;"> <p style="margin: 0;">CAS SHRC | Bridging Talents and Opportunities</p></td></tr></table><!--[if (gte mso 9)|(IE)]> </td></tr></table><![endif]--> </td></tr></table> </body></html>`
				// };

				// transporter.sendMail(mailOptions, function(error, info)
				// {
				  // if (error)
				  // {
					// console.log(error);
				  // } 
				  // else
				  // {
					// res.status(200).send(setting.status("Your payment successful, Please Check your Mail", true, "Student Paid", student._id))
				  // }
				// });
			  // }
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
          })
		  }
	  })
	  
	   .catch(err => {res.json(setting.status("Student Not Found", false, "error", err));});	   
  });					
})



// Todo : Pass token to get users details
// Todo : pagination
// *** GET *** /api/users/all *** Retrieve all users' basic details ***
router.get("/", middleware.checkToken, function (req, res, next)
{
  var aggregate = Student.aggregate();

  var page_no = req.param('page');
  var searchName = req.param('searchName');
  var searchEmal = req.param('searchEmal');
  var searchContact = req.param('searchContact');

    aggregate.sort({"createdAt" : -1})
            .lookup({ from: "colleges", localField: "college_id", foreignField: "_id",as: "college_doc"})
            .lookup({ from: "courses", localField: "course_id", foreignField: "_id",as: "course_doc"});


    if(searchName===null || searchName ===undefined)
    {
        
    }else
    {
        aggregate.match({"fname":{"$regex": searchName, "$options": "i"}});
    }

  if(searchEmal===null || searchEmal ===undefined)
    {
        
    }else
    {
        aggregate.match({"email":{"$regex": searchEmal, "$options": "i"}});
    }

  if(searchContact===null || searchContact ===undefined)
    {
        
    }else
    {
        aggregate.match({"contact_no":{"$regex": searchContact, "$options": "i"}});
    }
    
    if(page_no==0)
    {
        res.send(
        
            setting.status(validation.SHOW,false,"page No error",null)

        );
    }

    var options = { page : page_no, limit : setting.pagecontent}

    Student.aggregatePaginate(aggregate, options, function(err, results, pageCount, count) {
        if(err) 
        {
            
            res.send(
    
                setting.status("Error",false,"error",err)

            );
        }
        else
        { 
        
            res.send(
        
                setting.status("Details'",true,"Data found",{pages:pageCount,count:count,pagesize:Page_cont.pagecontent,results})

            );
        
        }
    })    
});


// *** GET *** /api/users/{userId} *** Retrieve one user's basic details ***
router.get("/:id", middleware.checkToken, function (req, res, next)
{
  var ObjectId = require('mongodb').ObjectID;

  const id = req.params.id;
  if(!id)
  {
    return res.status(200).send(setting.status("Student Id not found",false,"invalid id",null));
  }

  var aggregate = Student.aggregate();
    aggregate.sort({"createdAt" : -1})
            .match({_id:ObjectId(id)})
            .lookup({ from: "colleges", localField: "college_id", foreignField: "_id",as: "college_doc"})
            .lookup({ from: "courses", localField: "course_id", foreignField: "_id",as: "course_doc"});

    var options = { page : 1, limit : setting.pagecontent}

    Student.aggregatePaginate(aggregate, options, function(err, results, pageCount, count) {
        if(err) 
        {
            
            res.send(
    
                setting.status("Error",false,"error",err)

            );
        }
        else
        { 
        
            res.send(
        
                setting.status("Details'",true,"Data found",{results})

            );
        
        }
    })  
});

router.post("/stu/login", (req, res) =>{

  console.log(req.body);

  if(req.body.email==undefined || req.body.email==null || req.body.email=='') 
  {
      res.send(
      
        setting.status("Email acnnot be empty",false,"email empty",null)

    );
  }

  if(req.body.password==undefined || req.body.password==null || req.body.password=='') 
  {
      res.send(
      
        setting.status("Password cannot be empty",false,"password empty",null)

    );
  }

    var email = req.body.email;
    var password = req.body.password;

    Student.findOne({email: email}, function (err, user)
    {
        if (err)
            return res.send(
          
              setting.status("Try Again",false,"Error on server",err)
      
          );
        if (!user)
            return res.send(
          
              setting.status("Authentication false",false,"user not found",null)
      
          );


          if (user===null)
            return res.send(
          
              setting.status("User not find",false,"user not found",null)
      
          );

          console.log(user);
          console.log(req.body.password);
          console.log(user.password)
          

          var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
          if (!passwordIsValid)
                return res.send(
              
                  setting.status("Wrong Pasword",false,"password wrong",null)
          
              );

              let authority=[];
              let role='Student';

          Role_authority.find({role_id:user.role})//.populate('role_id')
          .then(result => {


              if(result.length>0)
              {
                //role=result[0].role_id.name;

                  for(var x=0;x<result.length;x++)
                  {
                      authority.push(result[x].authority)
                  }
              }else
              {
                  //return ("authority not found")
              }

            var token = jwt.sign({ id: user._id,name:user.fname,authority:authority,email:user.email,contact_no:user.contact_no,role:user.role,role_name:role  }, config.secretOrKey, {expiresIn: 86400});

            return res.send(
                
                  setting.status("Login success",true,"Authentication true",{"loginToken":token})        
                );
            });
    })


  });

router.post('/change_password/:id', middleware.checkToken,(req, res) => {

    
    var ObjectId = require('mongodb').ObjectID;
    var id=req.params.id;
	
	if(!ObjectId.isValid(id))
		{
			return res.send(
					
				setting.status(validation.FALSE,false,"incorrect id",null)

			 );
        }
        
    if(req.body.password==undefined || req.body.password==null || req.body.password=='') 
    {
        
        res.json(
                
            setting.status("Password cannot be empty",false,"password is empty",null)
    
          );
        
    }

    if(req.body.new_password==undefined || req.body.new_password==null || req.body.new_password=='') 
    {
        res.json(
                
            setting.status("New Password cannot be empty",false,"new password is empty",null)
    
          );
        
    }

    if(req.body.conform_password==undefined || req.body.conform_password==null || req.body.conform_password=='') 
    {
        res.json(
                
            setting.status("Conform Password cannot be empty",false,"conform password is empty",null)
    
          );
        
    }

    Student.findOne({
            _id: id
        })
        .then(college => {
            if (college) {


                var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
                if (!passwordIsValid)
                      res.send(
                    
                        setting.status("Wrong Pasword",false,"password wrong",null)
                
                    );

                let old_password=college.password;
                let coming_password=req.body.password;
                let new_password=req.body.new_password;
                let conform_password=req.body.conform_password;

                let hashedPassword="";


                bcrypt.genSalt(10, (err, salt) =>
                    {
                        bcrypt.hash(new_password, salt, (err, hash) =>
                        {
                            if (err)
                                throw err;
                                hashedPassword = hash;

                            })
                    });


                    if(new_password===conform_password)
                    {
                        Employer.findOneAndUpdate(
                            { _id : id },
                            {$set:{
                            password:hashedPassword}},
                            {runValidators: true, context: 'query' })
                          .then(college =>{
                                res.json(
            
                                    setting.status("Password Changed",true,"updated",null)
                            
                                );
                          })
                          .catch(err =>{
                                
                                if(err)
                                {
                                    res.json(
                
                                        setting.status("Error",false,"error",err)
                                
                                      );
                                }
                          });
                        
                    }else
                    {
                        res.json(
		
                            setting.status("Password Not Match",false,"newpassword not match to conform password",null)
                    
                        );
                    }

                
            } else {
                res.json(
		
                    setting.status("Employer Not Found",false,"error",err)
            
                  );
            }
        })
})





//*******/users/:userId******DELETE******Delete an account***********
router.delete("/:id",  middleware.checkToken,function(req, res, next)
{

  var ObjectId = require('mongodb').ObjectID;
    var id = req.params.id;
    
    if(!ObjectId.isValid(id))
        {
            return res.send(setting.status("Invalid Id",false,"incorrect id",null));
        }
        
    Student.findById(req.params.id).then(student => {
            // Delete
            student.remove().then(() => { 
                res.json(setting.status("Deleted",true,"deleted",null))});
          })
          .catch(err =>{
            res.json(setting.status("Student Not Found",false,"error",err));
          })
        }    
  );



  // const idToDelete = req.params.id;
  // // Validation not given id to delete
  // if(!idToDelete)
  // {
  //   return res.status(200).send(setting.status("Cannot find user",false,"id is not given",null));
  // }

  // // Find the id is exist and user not deleted already
  // Student.find({_id:idToDelete, is_deleted:false}).exec(function(err, results)
  // {
  //   if (err)
  //   res.status(200).json(setting.status("The id may not exist",false,"no id and is_delete false problem", err));

  //   // var countIsDelete = results.length;
  //   // console.log("the count is : " + countIsDelete);

  //   // // To the given id, is_delete should be false

  //   // if(countIsDelete > 0)
  //   if(results)
  //   {
  //     Student.findByIdAndUpdate(idToDelete, {is_deleted : true}, {new: true})
  //       .then(outputs => {
  //           if(!outputs)
  //           {
  //             return res.status(200).send(setting.status("Error in deleting user. Try anain",false,"Error in updating is_delete to true",null));
  //           }
  //           res.send(setting.status("User deleted successfully",true,"No problem user deleted. And is_delete changed to true",null));
  //       }).catch(err => {
  //           if(err.kind === 'ObjectId')
  //           {
  //             return res.status(200).send(setting.status("Id problem",false,"Error",err));          
  //           }
  //       });
  //   }
  //   if(!results)
  //       res.status(200).json(setting.status("Sorry user is not found",false,"The user may already deleted",null));
//   // }); 
// })





module.exports = router;
