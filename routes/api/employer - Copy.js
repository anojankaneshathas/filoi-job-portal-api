const express = require('express');
const router = express.Router();
//const passport = require('passport');
//Load Input Validation
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const Employer = require('../../models/employer');
const Invite = require('../../models/invite');
const setting=require("../return_msg/setting");
const employerValidation=require("../validation/employer");
let middleware = require('../../validation/middleware');
const Role_authority = require('../../models/role_authority');
const config = require('../../config/keys.js');
var nodemailer = require('nodemailer');
var generator = require('generate-password');
const Vacancy = require('../../models/vacancy');
const CCS_Associations = require('../../models/ccs_association');
const Vacancy_specialization = require('../../models/vacancy_specializations');
const Student = require('../../models/student_enrollment');
var sendMail=require("../return_msg/sendMail");
var unique = require('array-unique');


//@route GET api/employer/
//@desc Register route
//@access Public
router.post('/', middleware.checkToken,(req, res) => {

    // var result=middleware.function1("CAN_ADD_EMPLOYER");
    // if(!result.status)
    // {
    //     return res.send(result);
    // }
    console.log(req.body);

    var result=employerValidation.CheckValidation(req.body);
    if(!result.status)
    {
        return res.send(result);
    }

        Employer.findOne({
            email: req.body.email
        })
        .then(college => {
            if (college) {
                res.json(
		
                    setting.status("Email already exits",false,"email already exits",null)
            
                  );
            } else {

                var randPassword = generator.generate({
                    length: 8,
                    numbers: true
                  });
                  
                                 
                  //Hashing password 
                  var hashedPassword = bcrypt.hashSync(randPassword, 10); //Hashing password to unreadable

                const newCollege = new Employer({
                    name: req.body.name,
                    email: req.body.email,
                    contact_no:req.body.contact_no,
                    website:req.body.website,
                    address:req.body.address,
                    state: req.body.state,
                    city: req.body.city,
                    pin_code:req.body.pin_code,
                    hr_name:req.body.hr_name,
                    hr_email: req.body.hr_email,
                    hr_contact_no: req.body.hr_contact_no,
                    status:req.body.status,
                    //role:"5c45399c6b72f02628eb8c5b",
                    role:"5c45399c6b72f02628eb8c5b",
                    password:hashedPassword
                });

                

                            newCollege.save()
                            .then(college =>{

                                var transporter = nodemailer.createTransport({
                                    service: 'gmail',
                                    auth: {
                                      user: 'samplejobportal@gmail.com',
                                      pass: 'Job@1234'
                                    }
                                  });
                  
                                  var mailOptions = {
                                    from: 'samplejobportal@gmail.com',
                                    to: req.body.email,
                                    subject: 'Successfully Registered',
                                    html: `<!DOCTYPE html><html><head> <meta charset="utf-8"> <meta http-equiv="x-ua-compatible" content="ie=edge"> 
                                    <title>Registed</title> <meta name="viewport" content="width=device-width, initial-scale=1"> <style type="text/css"> /** * Google webfonts. Recommended to include the .woff version for cross-client compatibility. */ @media screen{@font-face{font-family: 'Source Sans Pro'; font-style: normal; font-weight: 400; src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format('woff');}@font-face{font-family: 'Source Sans Pro'; font-style: normal; font-weight: 700; src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format('woff');}}/** * Avoid browser level font resizing. * 1. Windows Mobile * 2. iOS / OSX */ body, table, td, a{-ms-text-size-adjust: 100%; /* 1 */ -webkit-text-size-adjust: 100%; /* 2 */}/** * Remove extra space added to tables and cells in Outlook. */ table, td{mso-table-rspace: 0pt; mso-table-lspace: 0pt;}/** * Better fluid images in Internet Explorer. */ img{-ms-interpolation-mode: bicubic;}/** * Remove blue links for iOS devices. */ a[x-apple-data-detectors]{font-family: inherit !important; font-size: inherit !important; font-weight: inherit !important; line-height: inherit !important; color: inherit !important; text-decoration: none !important;}/** * Fix centering issues in Android 4.4. */ div[style*="margin: 16px 0;"]{margin: 0 !important;}body{width: 100% !important; height: 100% !important; padding: 0 !important; margin: 0 !important;}/** * Collapse table borders to avoid space between cells. */ table{border-collapse: collapse !important;}a{color: #1a82e2;}img{height: auto; line-height: 100%; text-decoration: none; border: 0; outline: none;}</style></head><body style="background-color: #e9ecef;"> <div class="preheader" style="display: none; max-width: 0; max-height: 0; overflow: hidden; font-size: 1px; line-height: 1px; color: #fff; opacity: 0;"> 
                                    Registered successfully. </div><table border="0" cellpadding="0" cellspacing="0" width="100%"> <tr> <td align="center" bgcolor="#e9ecef"><!--[if (gte mso 9)|(IE)]> <table align="center" border="0" cellpadding="0" cellspacing="0" width="600"> <tr> <td align="center" valign="top" width="600"><![endif]--> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;"> <tr> <td align="center" valign="top" style="padding: 36px 24px;"> <a href="https://google.com" target="_blank" style="display: inline-block;"> <img src="http://172.104.40.142:3000/img/brand/logo-white.png" alt="Logo" border="0" width="200" style="display: block; width: 200px; max-width: 200px; min-width: px;"> </a> </td></tr></table><!--[if (gte mso 9)|(IE)]> </td></tr></table><![endif]--> </td></tr><tr> <td align="center" bgcolor="#e9ecef"><!--[if (gte mso 9)|(IE)]> <table align="center" border="0" cellpadding="0" cellspacing="0" width="600"> <tr> <td align="center" valign="top" width="600"><![endif]--> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;"> <tr> <td align="left" bgcolor="#ffffff" style="padding: 36px 24px 0; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; border-top: 3px solid #d4dadf;"> 
                                    <h1 style="margin: 0; font-size: 32px; font-weight: 700; letter-spacing: -1px; line-height: 48px;">
                                    Successfully Registered</h1> </td></tr></table><!--[if (gte mso 9)|(IE)]> </td></tr></table><![endif]--> </td></tr><tr> <td align="center" bgcolor="#e9ecef"><!--[if (gte mso 9)|(IE)]> <table align="center" border="0" cellpadding="0" cellspacing="0" width="600"> <tr> <td align="center" valign="top" width="600"><![endif]--> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;"> <tr> <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;"> <p style="margin: 0;">
                                    Hi ` + req.body.name + `, you have been successfully registered to the CAS Job Placements. 
                                    <p  style="color:black">Your username: `+req.body.email+` </p> <p>Password: `+randPassword+`</p></td></tr><tr> <td align="left" bgcolor="#ffffff"> <table border="0" cellpadding="0" cellspacing="0" width="100%"> <tr> <td align="center" bgcolor="#ffffff" style="padding: 12px;"> <table border="0" cellpadding="0" cellspacing="0"> <tr> <td align="center" bgcolor="#1a82e2" style="border-radius: 6px;"> <a href="https://google.com" target="_blank" style="display: inline-block; padding: 16px 36px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; color: #ffffff; text-decoration: none; border-radius: 6px;">
                                    GO TO APPLICATION</a> </td></tr></table> </td></tr></table> </td></tr></table><!--[if (gte mso 9)|(IE)]> </td></tr></table><![endif]--> </td></tr><tr> <td align="center" bgcolor="#e9ecef" style="padding: 24px;"><!--[if (gte mso 9)|(IE)]> <table align="center" border="0" cellpadding="0" cellspacing="0" width="600"> <tr> <td align="center" valign="top" width="600"><![endif]--> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;"> <tr> <td align="center" bgcolor="#e9ecef" style="padding: 12px 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #666;"> 
                                    <p style="margin: 0;"><b>CAS SHRC | Bridging Talents and Opportunities</b></p></td></tr></table><!--[if (gte mso 9)|(IE)]> </td></tr></table><![endif]--> </td></tr></table> </body></html>`
                                  };
                  
                                  transporter.sendMail(mailOptions, function(error, info){
                                    if (error) {
                                      console.log(error);
                                    } else {
                                      res.status(200).send(setting.status("Your payment successful, Please Check your Mail", true, "Student Paid", student._id))
                                      console.log('Email sent: ' + info.response);
                                    }
                                  });

                                res.json(
                
                                    setting.status("Employer created",true,"created",college)
                            
                                  );
                            })
                            .catch(err => {
                                
                                if(err.errors.email)
                                {
                                    res.json(
                
                                        setting.status("Employer Email Already Exits",false,"email unique",null)
                                
                                      );
                                }
        
                                if(err.errors.contact_no)
                                {
                                    res.json(
                
                                        setting.status("Employer Contact Number Already Exits",false,"contact_no unique",null)
                                
                                      );
                                }
        
                                if(err.errors.hr_email)
                                {
                                    res.json(
                
                                        setting.status("HR Email Already Exits",false,"hr_email unique",null)
                                
                                      );
                                }
        
                                if(err.errors.hr_contact_no)
                                {
                                    res.json(
                
                                        setting.status("HR Contact Number Already Exits",false,"hr_contact_no unique",null)
                                
                                      );
                                }
        
                                
                            });
                      
                
            }
        })
})

//@route  GET api/college/
//@desc  Get all  college
//@access Public
router.get('/', middleware.checkToken, (req, res) => {

    // var result=middleware.function1("CAN_VIEW_EMPLOYER");
    // if(!result.status)
    // {
    //     return res.send(result);
    // }


    var aggregate = Employer.aggregate();

    var page_no = req.param('page');
    var searchName = req.param('searchName');
	var searchEmail = req.param('searchEmail');
	var searchContact = req.param('searchContact');

    aggregate.sort({"createdAt" : -1})

    if(searchName===null || searchName ===undefined)
    {
        
    }else
    {
        aggregate.match({"name":{"$regex": searchName, "$options": "i"}});
    }
	
	if(searchEmail===null || searchEmail ===undefined)
    {
        
    }else
    {
        aggregate.match({"email":{"$regex": searchEmail, "$options": "i"}});
    }
	
	if(searchContact===null || searchContact ===undefined)
    {
        
    }else
    {
        aggregate.match({"contact_no":{"$regex": searchContact, "$options": "i"}});
    }
    
    if(page_no==0)
    {
        res.send(
        
            setting.status(validation.SHOW,false,"page No error",null)

        );
    }

    var options = { page : page_no, limit : setting.pagecontent}

    Employer.aggregatePaginate(aggregate, options, function(err, results, pageCount, count) {
        if(err) 
        {
            
            res.send(
    
                setting.status("Error",false,"error",err)

            );
        }
        else
        { 
        
            res.send(
        
                setting.status("Details'",true,"No data found",{pages:pageCount,count:count,pagesize:setting.pagecontent,results})

            );
        
        }
    })       
})

//@route  GET api/college/id
//@desc  Get one  college
//@access Public
router.get('/:id', middleware.checkToken,(req, res) => {

    // var result=middleware.function1("CAN_VIEW_EMPLOYER");
    // if(!result.status)
    // {
    //     return res.send(result);
    // }


    var ObjectId = require('mongodb').ObjectID;
    var id=req.params.id;
	
	if(!ObjectId.isValid(id))
		{
			return res.send(
					
				setting.status(validation.FALSE,false,"incorrect id",null)

			 );
        }
        
    var aggregate = Employer.aggregate();

    aggregate.match({"_id":ObjectId(id)})

    let page_no=req.params.page;                

    if(page_no==0)
    {
        res.send(
        
            setting.status(validation.SHOW,false,"page No error",null)

        );
    }

    var options = { page : page_no, limit : 6}

    Employer.aggregatePaginate(aggregate, options, function(err, results, pageCount, count) {
        if(err) 
        {
            res.send(
    
                setting.status("Error",false,"error",err)

            );
        }
        else
        { 
        
            res.send(
        
                setting.status("Details'",true,"No data found",{results})

            );
        
        }
    })       
})

// @route   DELETE api/college/:id
// @desc    Delete college
// @access  Private
router.delete(
    '/:id',
middleware.checkToken,    (req, res) => {

    // var result=middleware.function1("CAN_DELETE_EMPLOYER");
    // if(!result.status)
    // {
    //     return res.send(result);
    // }

    var ObjectId = require('mongodb').ObjectID;
    var id=req.params.id;
	
	if(!ObjectId.isValid(id))
		{
			return res.send(
					
				setting.status(validation.FALSE,false,"incorrect id",null)

			 );
        }
        
        Employer.findById(id).then(college => {
            // Delete
            college.delete().then(() => { 
                res.json(
		
                    setting.status("Deleted",true,"deleted",null)
        
              )});
          })
          .catch(err =>{
            res.json(
		
                setting.status("Employer Not Found",false,"error",err)
        
              );
          })
        }    
  );

//@route GET api/college/:id
//@desc Register route
//@access Public
router.post('/:id', middleware.checkToken,(req, res) => {

    // var result=middleware.function1("CAN_EDIT_EMPLOYER");
    // if(!result.status)
    // {
    //     return res.send(result);
    // }


    var resultVali=employerValidation.CheckValidation(req.body);
    if(!resultVali.status)
    {
        return res.send(resultVali);
    }
    
    var ObjectId = require('mongodb').ObjectID;
    var id=req.params.id;
	
	if(!ObjectId.isValid(id))
		{
			return res.send(
					
				setting.status(validation.FALSE,false,"incorrect id",null)

			 );
		}

    Employer.findOne({
            _id: id
        })
        .then(college => {
            if (college) {
                Employer.findOneAndUpdate(
                    { _id : id },
                    {$set:{
                    name: req.body.name,
                    email: req.body.email,
                    contact_no:req.body.contact_no,
                    website:req.body.website,
                    address:req.body.address,
                    state: req.body.state,
                    city: req.body.city,
                    pin_code:req.body.pin_code,
                    hr_name:req.body.hr_name,
                    hr_email: req.body.hr_email,
                    hr_contact_no: req.body.hr_contact_no,
                    status:req.body.status}},
                    {runValidators: true, context: 'query' })
                  .then(college =>{
                    res.json(
		
                        setting.status("Employer Updated",true,"updated",college)
                
                      );
                  })
                  .catch(err =>{
                        if(err.errors.email)
                        {
                            res.json(
		
                                setting.status("Employer Email Already Exits",false,"email unique",null)
                        
                              );
                        }

                        if(err.errors.contact_no)
                        {
                            res.json(
		
                                setting.status("Employer Contact Number Already Exits",false,"contact_no unique",null)
                        
                              );
                        }

                        if(err.errors.hr_email)
                        {
                            res.json(
		
                                setting.status("HR Email Already Exits",false,"hr_email unique",null)
                        
                              );
                        }

                        if(err.errors.hr_contact_no)
                        {
                            res.json(
		
                                setting.status("HR Contact Number Already Exits",false,"hr_contact_no unique",null)
                        
                              );
                        }
                  });
            } else {
                res.json(
		
                    setting.status("Employer Not Found",false,"error",err)
            
                  );
            }
        })
})


router.post('/change_password/:id', middleware.checkToken,(req, res) => {

    
    var ObjectId = require('mongodb').ObjectID;
    var id=req.params.id;
	
	if(!ObjectId.isValid(id))
		{
			return res.send(
					
				setting.status(validation.FALSE,false,"incorrect id",null)

			 );
        }
        
    if(req.body.password==undefined || req.body.password==null || req.body.password=='') 
    {
        
        res.json(
                
            setting.status("Password cannot be empty",false,"password is empty",null)
    
          );
        
    }

    if(req.body.new_password==undefined || req.body.new_password==null || req.body.new_password=='') 
    {
        res.json(
                
            setting.status("New Password cannot be empty",false,"new password is empty",null)
    
          );
        
    }

    if(req.body.conform_password==undefined || req.body.conform_password==null || req.body.conform_password=='') 
    {
        res.json(
                
            setting.status("Conform Password cannot be empty",false,"conform password is empty",null)
    
          );
        
    }

    Employer.findOne({ _id: id})
        .then(college => {
            if (college) {

                let old_password=college.password;
                let coming_password=req.body.password;
                let new_password=req.body.new_password;
                let conform_password=req.body.conform_password;

                var passwordIsValid = bcrypt.compareSync(req.body.password, college.password);
                if (!passwordIsValid)
                    res.send(
                    
                        setting.status("Wrong Pasword",false,"password wrong",null)
                
                    );

                let hashPassword="";

                    if(new_password===conform_password)
                    {
                        bcrypt.genSalt(10, (err, salt) =>
                            {
                                bcrypt.hash(new_password, salt, (err, hash) =>
                                {
                                    if (err)
                                        throw err;
                                        hashPassword = hash;

                                        Employer.findOneAndUpdate(
                                            { _id : id },
                                            {$set:{
                                            password:hashPassword,
                                            is_password_defalut:0
                                            }},
                                            {runValidators: true, context: 'query' })
                                          .then(college =>{
                                                res.json(
                            
                                                    setting.status("Password Changed",true,"updated",null)
                                            
                                                );
                                          })
                                          .catch(err =>{
                                                
                                                if(err)
                                                {
                                                    res.json(
                                
                                                        setting.status("Error",false,"error",err)
                                                
                                                      );
                                                }
                                          });

                                })
                            });
                       
                        
                    }else
                    {
                        res.json(
		
                            setting.status("Password Not Match",false,"newpassword not match to conform password",null)
                    
                        );
                    }

                
            } else {
                res.json(
		
                    setting.status("Employer Not Found",false,"error",err)
            
                  );
            }
        })
})


router.post('/change_status/:id', middleware.checkToken,(req, res) => {

    // var result=middleware.function1("CAN_EDIT_EMPLOYER");
    // if(!result.status)
    // {
    //     return res.send(result);
    // }

    console.log("mano");

    
    var ObjectId = require('mongodb').ObjectID;
    var id=req.params.id;
	
	if(!ObjectId.isValid(id))
		{
			return res.send(
					
				setting.status(validation.FALSE,false,"incorrect id",null)

			 );
		}

    Employer.findOne({
            _id: id
        })
        .then(college => {
            if (college) {
                Employer.findOneAndUpdate(
                    { _id : id },
                    {$set:{
                    status:req.body.status}},
                    {runValidators: true, context: 'query' })
                  .then(college =>{
                    res.json(
		
                        setting.status("Employer Status Updated",true,"updated",college.status)
                
                      );
                  })
                  .catch(err =>{
                        
                        if(err)
                        {
                            res.json(
		
                                setting.status("Error",false,"error",err)
                        
                              );
                        }
                  });
            } else {
                res.json(
		
                    setting.status("Employer Not Found",false,"error",err)
            
                  );
            }
        })
})

router.post("/emp/login", (req, res) =>{

    console.log(req.body);
  
    if(req.body.email==undefined || req.body.email==null || req.body.email=='') 
    {
        res.send(
        
          setting.status("Email acnnot be empty",false,"email empty",null)
  
      );
    }

  
    if(req.body.password==undefined || req.body.password==null || req.body.password=='') 
    {
        res.send(
        
          setting.status("Password cannot be empty",false,"password empty",null)
  
      );
    }
  
      var email = req.body.email;
      var password = req.body.password;
  
      Employer.findOne({email: email}, function (err, user)
      {
          if (err)
              return res.send(
            
                setting.status("Try Again",false,"Error on server",err)
        
            );
  
          if (!user)
              return res.send(
            
                setting.status("Authentication false",false,"user not found",null)
        
            );
  
            if (!user)
               returnres.send(
            
                setting.status("Authentication false",false,"user not found",null)
        
            );
  
            var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
            if (!passwordIsValid)
                  res.send(
                
                    setting.status("Wrong Pasword",false,"password wrong",null)
            
                );
  
  
                let authority=[];
                let role='Employer';
  
            Role_authority.find({role_id:user.role})//.populate('role_id')
            .then(result => {
               // role=result[0].role_id.name;
                if(result.length>0)
                {
                    for(var x=0;x<result.length;x++)
                    {
                        authority.push(result[x].authority)
                    }
                }else
                {
                    //return ("authority not found")
                }
  
              var token = jwt.sign({ id: user._id,name:user.fname,authority:authority,email:user.email,contact_no:user.contact_no,role:user.role,role_name:role,default_password:user.is_password_defalut }, config.secretOrKey, {expiresIn: 86400});
  
              res.send(
                  
                    setting.status("Login success",true,"Authentication true",{"loginToken":token})        
                  );
              });
      })
    })




//********************************************************* */ Vacancy/********************************************** */

//********************************************************************************************************************** */



router.get('/suggestion/:vacancy_id',async (req, res) => { //middleware.checkToken,

    // var result=middleware.function1("CAN_VIEW_UNIVERSITIES");
    // if(!result.status)
    // {
    //     return res.send(result);
    // }

    console.log("mano");
    

    var ObjectId = require('mongodb').ObjectID;
    var vacancy_id=req.params.vacancy_id;
	
	if(!ObjectId.isValid(vacancy_id))
		{
			return res.send(
					
				setting.status("Incorrect ID",false,"incorrect id",null)

			 );
		}
   // var aggregate = Vacancy.aggregate();
   var aggregate = Student.aggregate();

  // aggregate.match({"_id":ObjectId(id)})

//    aggregate.match({"_id":ObjectId(id)})
//    .lookup({ from: "vacancy_specializations", localField: "_id", foreignField: "vacancy_id",as: "vacancy_spec_doc"})
//    .lookup({ from: "ccs_associations", localField: "vacancy_spec_doc.specialization_id", foreignField: "specialisation_id",as: "spec_doc"})
//    .lookup({ from: "student_enrollments", localField: "spec_doc.course_id", foreignField: "course_id",as: "student_doc"})
//    .project({vacancy_spec_doc:0})
//    .project({spec_doc:0})
  // .project({students:"$student_doc"});

//finalllllllll
    //  aggregate//.match({"_id":ObjectId(id)})
    //  .lookup({ from: "ccs_associations", localField: "course_id", foreignField: "course_id",as: "ccs_doc"})
    //  .lookup({ from: "vacancy_specializations", localField: "ccs_doc.specialisation_id", foreignField: "specialization_id",as: "vacancy_spec_doc"})
    // //  .filter({
    //     input: "$ccs_doc",
    //     as: "s",
    //     cond: {
    //         $eq: ["$$s.specialisation_id", "$vacancy_spec_doc.specialization_id"]
    //     },
        
    // })

     //.lookup({ from: "vacancies", localField: "vacancy_spec_doc.vacancy_id", foreignField: "_id",as: "vacancy_doc"})
    //  .project({ccs_doc:0})
    // .project({vacancy_spec_doc:0})
    // .project({vacancy_doc:0})
    //.match({"vacancy_spec_doc.specialization_id":ObjectId("5c2468b6ef69ab1c809488a9")})

    //.match({"vacancy_spec_doc.specialization_id":"ccs_doc.specialisation_id"})
    // .lookup({ from: "vacancy_specializations", localField: "_id", foreignField: "vacancy_id",as: "vacancy_spec_doc"})
    // .lookup({ from: "ccs_associations", localField: "vacancy_spec_doc.specialization_id", foreignField: "specialisation_id",as: "spec_doc"})
    // .lookup({ from: "student_enrollments", localField: "spec_doc.course_id", foreignField: "course_id",as: "student_doc"})
    
    let specialization=[];
    let course=[];
    let student=[];


    Vacancy_specialization.find({vacancy_id:vacancy_id})
    .then(async result => {

        for(let x=0;x<result.length;x++)
        {
            specialization.push(result[x].specialization_id)
        }


        for(let x=0;x<specialization.length;x++)
        {
            await CCS_Associations.find({specialisation_id:specialization[x]})
            .then(async result => {

                for(let x=0;x<result.length;x++)
                {
                    await course.push({"id":result[x].course_id})
                }

            });
        }

        let test = removeDuplicates(course, "fake");
        console.log(test);

        function removeDuplicates(myArr, prop) {
        return myArr.filter((obj, pos, arr) => {
        return arr.map(mapObj =>
        mapObj[prop]).indexOf(obj[prop]) === pos;
        });
        }
        
        for(let x=0;x<test.length;x++)
        {
            await Student.find({course_id:test[x].id})
            .then(async result => {

                for(let x=0;x<result.length;x++)
                {
                    await student.push(result[x])
                    console.log(student)
                }

            });
        }

        let new_student=[];

        new_student=unique(student)

        return res.send(
    
            setting.status("Student",true,"students",new_student)

        );
    });   

    // let page_no=req.params.page;                

    // if(page_no==0)
    // {
    //     res.send(
        
    //         setting.status("Error",false,"page No error",null)

    //     );
    // }

    // var options = { page : page_no, limit : 6}

    // await Student.aggregatePaginate(student, options, function(err, results, pageCount, count) {
    //     if(err) 
    //     {
    //         console.log(err)
    //         res.send(
    
    //             setting.status("Error",false,"error",err)

    //         );
    //     }
    //     else
    //     { 
        
    //         res.send(
        
    //             setting.status("Details'",true,"No data found",{pageCount:pageCount,results})

    //         );
        
    //     }
    // })       
})


router.post('/vacancy/:vacancy_id/student/:student_id/invite', middleware.checkToken,(req, res) => {

    // var result=middleware.function1("CAN_ADD_EMPLOYER");
    // if(!result.status)
    // {
    //     return res.send(result);
    // }

    if(req.params.student_id==undefined || req.params.student_id==null || req.params.student_id=='') 
        {
            return res.json(

                setting.status("Student ID cannot be empty",false,"student_id is empty",null)
        
            );
        }

    if(req.params.vacancy_id==undefined || req.params.vacancy_id==null || req.params.vacancy_id=='') 
        {
            return res.json(

                setting.status("Vacancy ID cannot be empty",false,"vacancy_id is empty",null)
        
            );
        }

    var ObjectId = require('mongodb').ObjectID;

	if(!ObjectId.isValid(req.params.vacancy_id))
        {
            return res.send(
                        
                setting.status("Vacancy ID wrong","False","object id wrong",null)

            );
        }

    if(!ObjectId.isValid(req.params.student_id))
        {
            return res.send(
                        
                setting.status("Student ID wrong","False","object id wrong",null)

            );
        }

    var student_id=req.params.student_id;
    var vacancy_id=req.params.vacancy_id;

    const newInvite = new Invite({
    student_id: student_id,
    vacancy_id: vacancy_id,
    status:'active',
    is_accepted:false,
    });

        newInvite.save()
        .then(college =>{

           
                var html;

                
                Student.find({_id:student_id})
                .then(result => {

                    var email=result[0].email;
                    var name=result[0].fname;

                Vacancy.find({_id:vacancy_id})
                .then(result => {

                    var job_title=result[0].job_title;

                    
                    html=`<!DOCTYPE html><html><head> <meta charset="utf-8"> <meta http-equiv="x-ua-compatible" content="ie=edge"> 
                    <title>Registed</title> <meta name="viewport" content="width=device-width, initial-scale=1"> <style type="text/css"> /** * Google webfonts. Recommended to include the .woff version for cross-client compatibility. */ @media screen{@font-face{font-family: 'Source Sans Pro'; font-style: normal; font-weight: 400; src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format('woff');}@font-face{font-family: 'Source Sans Pro'; font-style: normal; font-weight: 700; src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format('woff');}}/** * Avoid browser level font resizing. * 1. Windows Mobile * 2. iOS / OSX */ body, table, td, a{-ms-text-size-adjust: 100%; /* 1 */ -webkit-text-size-adjust: 100%; /* 2 */}/** * Remove extra space added to tables and cells in Outlook. */ table, td{mso-table-rspace: 0pt; mso-table-lspace: 0pt;}/** * Better fluid images in Internet Explorer. */ img{-ms-interpolation-mode: bicubic;}/** * Remove blue links for iOS devices. */ a[x-apple-data-detectors]{font-family: inherit !important; font-size: inherit !important; font-weight: inherit !important; line-height: inherit !important; color: inherit !important; text-decoration: none !important;}/** * Fix centering issues in Android 4.4. */ div[style*="margin: 16px 0;"]{margin: 0 !important;}body{width: 100% !important; height: 100% !important; padding: 0 !important; margin: 0 !important;}/** * Collapse table borders to avoid space between cells. */ table{border-collapse: collapse !important;}a{color: #1a82e2;}img{height: auto; line-height: 100%; text-decoration: none; border: 0; outline: none;}</style></head><body style="background-color: #e9ecef;"> <div class="preheader" style="display: none; max-width: 0; max-height: 0; overflow: hidden; font-size: 1px; line-height: 1px; color: #fff; opacity: 0;"> 
                    Registered successfully. </div><table border="0" cellpadding="0" cellspacing="0" width="100%"> <tr> <td align="center" bgcolor="#e9ecef"><!--[if (gte mso 9)|(IE)]> <table align="center" border="0" cellpadding="0" cellspacing="0" width="600"> <tr> <td align="center" valign="top" width="600"><![endif]--> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;"> <tr> <td align="center" valign="top" style="padding: 36px 24px;"> <a href="https://google.com" target="_blank" style="display: inline-block;"> <img src="http://172.104.40.142:3000/img/brand/logo-white.png" alt="Logo" border="0" width="200" style="display: block; width: 200px; max-width: 200px; min-width: px;"> </a> </td></tr></table><!--[if (gte mso 9)|(IE)]> </td></tr></table><![endif]--> </td></tr><tr> <td align="center" bgcolor="#e9ecef"><!--[if (gte mso 9)|(IE)]> <table align="center" border="0" cellpadding="0" cellspacing="0" width="600"> <tr> <td align="center" valign="top" width="600"><![endif]--> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;"> <tr> <td align="left" bgcolor="#ffffff" style="padding: 36px 24px 0; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; border-top: 3px solid #d4dadf;"> 
                    <h1 style="margin: 0; font-size: 32px; font-weight: 700; letter-spacing: -1px; line-height: 48px;">
                    Invite a new job</h1> </td></tr></table><!--[if (gte mso 9)|(IE)]> </td></tr></table><![endif]--> </td></tr><tr> <td align="center" bgcolor="#e9ecef"><!--[if (gte mso 9)|(IE)]> <table align="center" border="0" cellpadding="0" cellspacing="0" width="600"> <tr> <td align="center" valign="top" width="600"><![endif]--> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;"> <tr> <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;"> <p style="margin: 0;">
                    Hi ` + name + `, you have invite to the post of ` + job_title + `. 
                    </td></tr><tr> <td align="left" bgcolor="#ffffff"> <table border="0" cellpadding="0" cellspacing="0" width="100%"> <tr> <td align="center" bgcolor="#ffffff" style="padding: 12px;"> <table border="0" cellpadding="0" cellspacing="0"> <tr> <td align="center" bgcolor="#1a82e2" style="border-radius: 6px;"> <a href="https://google.com" target="_blank" style="display: inline-block; padding: 16px 36px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; color: #ffffff; text-decoration: none; border-radius: 6px;">
                    GO TO APPLICATION</a> </td></tr></table> </td></tr></table> </td></tr></table><!--[if (gte mso 9)|(IE)]> </td></tr></table><![endif]--> </td></tr><tr> <td align="center" bgcolor="#e9ecef" style="padding: 24px;"><!--[if (gte mso 9)|(IE)]> <table align="center" border="0" cellpadding="0" cellspacing="0" width="600"> <tr> <td align="center" valign="top" width="600"><![endif]--> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;"> <tr> <td align="center" bgcolor="#e9ecef" style="padding: 12px 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #666;"> 
                    <p style="margin: 0;"><b>CAS SHRC | Bridging Talents and Opportunities</b></p></td></tr></table><!--[if (gte mso 9)|(IE)]> </td></tr></table><![endif]--> </td></tr></table> </body></html>`
                

                        sendMail.sendMail(email,html,'Job Invitite')
                        
                    });
                });   
                
            res.json(

                setting.status("Succesfully Invited",true,"invited",college)
        
                );
        })
        .catch(err => {
            
            if(err)
            {
                res.json(

                    setting.status("Error",false,"error",err)
            
                    );
            }
        });
})




module.exports = router;